#include "stdafx.h"
#include "DuanRpcChannel.h"
#include "RemoteCall.pb.h"
#include <zmq.h>


CDuanRpcChannel::CDuanRpcChannel(std::shared_ptr<BaseClientCommon> common)
{
	m_baseCommon = common;
	m_baseCommon->Front();
}


CDuanRpcChannel::~CDuanRpcChannel()
{
}

uint64_t CDuanRpcChannel::GetIdentify()
{
	static std::mutex identifyMutex;
	identifyMutex.lock();

	static uint64_t  uint64Identify = 0;

	uint64Identify++;
	return uint64Identify;
}

void CDuanRpcChannel::CallMethod(const google::protobuf::MethodDescriptor* method,
	google::protobuf::RpcController* controller,
	const google::protobuf::Message* request,
	google::protobuf::Message* response,
	google::protobuf::Closure* done)
{
	//组建request
	bool bRet = false;
	Duan::DuanRpcMessage message;
	message.set_service(method->service()->name());
	message.set_method(method->name());
	message.set_request(request->SerializeAsString());
	message.set_type(Duan::DuanRpcMessageType::REQUEST);
	message.set_code(Duan::DuanRpcErrorCode::_NO_ERROR);
	message.set_identify(GetIdentify());

	//发送数据
	int nByteSize = message.ByteSize();
	char *pBuffer = new char[nByteSize];
	bRet = message.SerializeToArray(pBuffer, nByteSize);
	std::shared_ptr<char> pRequestBuffer(pBuffer);
	if (false == bRet)
	{
		controller->SetFailed("请求参数序列化失败");
		return;
	}

	bRet = m_baseCommon->Send(pRequestBuffer, nByteSize);
	if (false == bRet)
	{
		controller->SetFailed("网络发送失败");
		return;
	}


	////接受数据
	int nRecvSize = 0;
	char *pResponseBuf = new char[RECV_MSG_MAX_BUFFER];
	std::shared_ptr<char> pResponseBuffer(pResponseBuf);
	bRet = m_baseCommon->Recv(pResponseBuffer, nRecvSize, RECV_MSG_MAX_BUFFER);
	if (false == bRet)
	{
		controller->SetFailed("网络接收失败");
		return;
	}


	//解析返回数据
	Duan::DuanRpcMessage messageResponse;
	messageResponse.ParseFromArray(pResponseBuffer.get(), nRecvSize);
	if (::Duan::DuanRpcMessageType::RESPONSE != messageResponse.type()) //返回消息不是回复报文
	{
		controller->SetFailed("返回消息不是回复报文");
		return;
	}
	if (Duan::DuanRpcErrorCode::_NO_ERROR != messageResponse.code()) //远程调用错误
	{
		controller->SetFailed("远程调用出错");
		return;
	}

	//得到response
	bRet = response->ParseFromArray(messageResponse.response().c_str(), messageResponse.response().size());
	if (false == bRet)
	{
		controller->SetFailed("返回报文序列化失败");
		return;
	}
	return;
}