#include "stdafx.h"
#include "ZmqServer.h"
#include "RemoteCall.pb.h"
#include "DuanRpcController.h"
#include <zmq.h>

ZmqServer::ZmqServer(std::string strListen, std::shared_ptr<ServiceManager> serviceManager)
{
	m_context = nullptr;
	m_listenSocket = nullptr;
	m_strLieten = strListen;
	m_serviceManager = serviceManager;
	m_uintTimeout = 500;
}


ZmqServer::~ZmqServer()
{
	if (nullptr != m_listenSocket)
	{
		zmq_close(m_listenSocket);
		m_listenSocket = nullptr;
	}

	if (nullptr != m_listenSocket)
	{
		zmq_term(m_context);
		m_context = nullptr;
	}
}

//使用之前初始化操作
bool ZmqServer::Front()
{
	int nRet = 0;
	m_context = zmq_init(1);
	if (nullptr == m_context)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		return false;
	}
	m_listenSocket = zmq_socket(m_context, ZMQ_REP);
	if (nullptr == m_listenSocket)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		zmq_term(m_context);
		m_context = nullptr;
		return false;
	}
	nRet = zmq_setsockopt(m_listenSocket, ZMQ_SNDTIMEO, &m_uintTimeout, sizeof(m_uintTimeout));
	if (nRet < 0)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		zmq_close(m_listenSocket);
		m_listenSocket = nullptr;
		zmq_term(m_context);
		m_context = nullptr;
		return false;
	}

	return true;
}

//发送数据
bool ZmqServer::Listen()
{
	int nRet = 0;

	nRet = zmq_bind(m_listenSocket, m_strLieten.c_str());
	if (nRet < 0)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		return false;
	}
	while (1)
	{
		//接受部分
		zmq_msg_t recv_msg;
		zmq_msg_init(&recv_msg);
		if (zmq_msg_recv(&recv_msg, m_listenSocket, 0) < 0)                            //0表示非阻塞
		{
			Sleep(200);
			continue;
		}
		else
		{
			Duan::DuanRpcMessage message;
			DuanRpcController controller;
			Duan::DuanRpcMessage messageResponse;
			bool bRet = false;

			message.ParseFromArray(zmq_msg_data(&recv_msg), zmq_msg_size(&recv_msg));
			std::string strService = message.service();
			std::string strMethod = message.method();

			//查找对应的接口类型
			google::protobuf::Service *service = m_serviceManager->GetService(strService);
			google::protobuf::MethodDescriptor* method = m_serviceManager->GetMethod(strService, strMethod);
			google::protobuf::Message *request = service->GetRequestPrototype(method).New();
			google::protobuf::Message *response = service->GetResponsePrototype(method).New();

			if (nullptr == service)
			{
				messageResponse.set_code(Duan::DuanRpcErrorCode::NO_SERVICE);
			}
			else if (nullptr == method)
			{
				messageResponse.set_code(Duan::DuanRpcErrorCode::NO_METHOD);
			}
			else
			{
				//开始调用
				bRet = request->ParseFromArray(message.request().c_str(), message.request().size());
				if (false == bRet)
				{
					messageResponse.set_code(Duan::DuanRpcErrorCode::INVALID_REQUEST);
				}
				service->CallMethod(method, &controller, request, response, nullptr);
			}


			//组返回报文
			messageResponse.set_type(::Duan::DuanRpcMessageType::RESPONSE);	
			messageResponse.set_code(Duan::DuanRpcErrorCode::_NO_ERROR);
			messageResponse.set_identify(message.identify());

			//序列化Response
			int nByteSize = message.ByteSize();
			char *pBuffer = new char[nByteSize];
			std::shared_ptr<char> pResponseBuffer(pBuffer);
			response->SerializeToArray(pBuffer, nByteSize);
			messageResponse.set_response(pBuffer);


			//将整个返回报文序列化
			nByteSize = messageResponse.ByteSize();
			zmq_msg_t sendMsg;
			zmq_msg_init_size(&sendMsg, nByteSize);
			messageResponse.SerializeToArray(zmq_msg_data(&sendMsg), nByteSize);

			//发送返回报文	
			zmq_msg_send(&sendMsg, m_listenSocket, 0);

			//清空参数列表
			request->Clear();
			response->Clear();
		}
	}
}

//使用之后的操作
bool ZmqServer::Back()
{
	return true;
}