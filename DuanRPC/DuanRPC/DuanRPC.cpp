// DuanRPC.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "CallHelloClient.h"
#include "ZmqCommon.h"
#include "ServiceManager.h"
#include "CallHelloService.h"
#include "ZmqServer.h"
#include "TcpCommon.h"
#include "TcpServer.h"

int main()
{
	printf("input s, or c\n");
	char c = getchar();
	if ('s' == c)
	{
		std::shared_ptr<ServiceManager> serviceManager(new ServiceManager());

		//注册远程调用函数
		CallHelloService callHelloService;
		serviceManager->RegisterRpcService(&callHelloService);
		//ZmqServer server("tcp://*:8887", serviceManager);
		TcpServer server(8887, serviceManager);
		server.Front();
		server.Listen();
		server.Back();
	}
	else if ('c' == c)
	{
		std::shared_ptr<BaseClientCommon> common(new TcpCommon("127.0.0.1", 8887));
		//std::shared_ptr<BaseClientCommon> common(new ZmqCommon("tcp://127.0.0.1:60000"));
		std::shared_ptr<CDuanRpcChannel> channel(new CDuanRpcChannel(common));
		CallHelloClient client(channel);
		client.SayHellp("XXXXXXXXXXXXXXxx");
	}
	else
	{


	}

	printf("programer close\n");
	c = getchar();
    return 0;
}

