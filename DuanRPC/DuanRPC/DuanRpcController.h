#pragma once

#include <google\protobuf\service.h>

class DuanRpcController :public google::protobuf::RpcController
{
public:
	DuanRpcController();
	~DuanRpcController();

	virtual void Reset();

	virtual bool Failed() const;

	virtual std::string ErrorText() const;



	virtual void SetFailed(const std::string& reason);


	/***************下面三个函数，仅仅适用于当我们会取消调用的时候。才需要，因此直接设置为空***************/
	virtual void StartCancel() {};

	virtual bool IsCanceled() const { return false; };

	virtual void NotifyOnCancel(google::protobuf::Closure* callback) {};
private:
	bool m_bIsFailed;
	std::string m_strErrorText;
};

