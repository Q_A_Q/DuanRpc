#pragma once

#include "RemoteCall.pb.h"
#include "DuanRpcChannel.h"
class CallHelloService: public Duan::CallHello_RPC
{
public:
	CallHelloService() {
	}
	~CallHelloService();

	virtual void SayHello(::google::protobuf::RpcController* controller,
		const ::Duan::CallHello_Request* request,
		::Duan::CallHello_Response* response,
		::google::protobuf::Closure* done) override;


};

