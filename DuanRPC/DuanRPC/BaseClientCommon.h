#pragma once
#include <memory>

#define RECV_MSG_MAX_BUFFER 1024 * 1024 * 16

class BaseClientCommon
{
public:
	BaseClientCommon() {};
	~BaseClientCommon() {};

	//使用之前初始化操作
	virtual bool Front() = 0;

	//发送数据
	virtual bool Send(std::shared_ptr<char> pBuffer, int nLen) = 0;

	//接受数据
	virtual bool Recv(std::shared_ptr<char> pBuffer, int &nLen, int nBufferSize) = 0;

	//使用之后的操作
	virtual bool Back() = 0;

private:
	bool bThreadIsWork;
};

