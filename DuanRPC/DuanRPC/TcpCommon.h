#pragma once
#include "BaseClientCommon.h"
#include <memory>
#include <string>
#include "Tcp.h"

class TcpCommon :
	public BaseClientCommon
{
public:
	TcpCommon(std::string strConnect, int nPort);
	~TcpCommon();

	bool Front();

	bool Send(std::shared_ptr<char> pBuffer, int nLen);

	bool Recv(std::shared_ptr<char> pBuffer, int &nLen, int nBufferSize);

	bool Back();


private:
	std::string m_strConnect;
	int m_uintTimeout;
	int m_uintPort;
	CTcp m_nTcp;

};

