#pragma once

#include <google\protobuf\service.h>

#define MAX_SERVICE_METHODs 10

class ServiceManager
{
public:
	ServiceManager();
	~ServiceManager();

	bool RegisterRpcService(google::protobuf::Service *rpc_service);


	google::protobuf::Service *GetService(std::string service);
	google::protobuf::MethodDescriptor *GetMethod(std::string service,std::string method);


private:
	std::map<std::string, google::protobuf::Service*> m_mapService;
};

