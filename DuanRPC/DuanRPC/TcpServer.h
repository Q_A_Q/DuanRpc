#pragma once

#include "ServiceManager.h"
#include "BaseServerCommon.h"
#include "Tcp.h"
#include "boost\thread.hpp"

class TcpServer :
	public BaseServerCommon
{
public:
	TcpServer(int nPort, std::shared_ptr<ServiceManager> serviceManager);
	~TcpServer();

	//使用之前初始化操作
	bool Front();

	//发送数据
	bool Listen();

	//使用之后的操作
	bool Back();

	void ThreadWork(SOCKET socket);

private:
	std::shared_ptr<ServiceManager> m_serviceManager;
	int m_nPort;
	CTcp m_nTcp;
	boost::thread_group m_threadGroup;
};

