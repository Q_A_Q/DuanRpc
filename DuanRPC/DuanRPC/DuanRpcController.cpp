#include "stdafx.h"
#include "DuanRpcController.h"


DuanRpcController::DuanRpcController()
{
}


DuanRpcController::~DuanRpcController()
{
}

void DuanRpcController::Reset()
{
	m_bIsFailed = false;
	m_strErrorText = "";
}

bool DuanRpcController::Failed() const
{
	return m_bIsFailed == true;
}

std::string DuanRpcController::ErrorText() const
{
	return m_strErrorText;
}

void DuanRpcController::SetFailed(const std::string& reason)
{
	m_bIsFailed = true;
	m_strErrorText = reason;
}