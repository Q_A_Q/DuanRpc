#include "stdafx.h"
#include "BlockQueue.h"

template<typename T>
class BlockingQueue<T>::sync {
public:
	mutable boost::mutex mutex_;
	boost::condition_variable condition_;
};

template<typename T>
BlockQueue<T>::BlockQueue()
	: sync_(new sync()) {
}

template<typename T>
void BlockQueue<T>::push(const T& t) {
	boost::mutex::scoped_lock lock(sync_->mutex_);
	queue_.push(t);
	lock.unlock();
	sync_->condition_.notify_one();
}

template<typename T>
bool BlockQueue<T>::try_pop(T* t) {
	boost::mutex::scoped_lock lock(sync_->mutex_);

	if (queue_.empty()) {
		return false;
	}

	*t = queue_.front();
	queue_.pop();
	return true;
}

template<typename T>
T BlockQueue<T>::pop(const string& log_on_wait) {
	boost::mutex::scoped_lock lock(sync_->mutex_);

	while (queue_.empty()) {
		if (!log_on_wait.empty()) {
			LOG_EVERY_N(INFO, 1000) << log_on_wait;
		}
		sync_->condition_.timed_wait(lock, boost::get_system_time() + boost::posix_time::seconds(3));
	}

	T t = queue_.front();
	queue_.pop();
	return t;
}

template<typename T>
bool BlockQueue<T>::try_peek(T* t) {
	boost::mutex::scoped_lock lock(sync_->mutex_);

	if (queue_.empty()) {
		return false;
	}

	*t = queue_.front();
	return true;
}

template<typename T>
T BlockQueue<T>::peek() {
	boost::mutex::scoped_lock lock(sync_->mutex_);

	while (queue_.empty()) {
		sync_->condition_.wait(lock);
	}

	return queue_.front();
}

template<typename T>
size_t BlockQueue<T>::size() const {
	boost::mutex::scoped_lock lock(sync_->mutex_);
	return queue_.size();
}
