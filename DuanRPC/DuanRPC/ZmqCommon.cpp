#include "stdafx.h"
#include "ZmqCommon.h"


ZmqCommon::ZmqCommon(std::string strConnect)
{
	m_strConnect = strConnect;
	m_context = nullptr;
	m_socket = nullptr;
	m_uintTimeout = 10 * 1000;
}


ZmqCommon::~ZmqCommon()
{
	if (nullptr != m_context)
	{
		zmq_term(m_context);
		m_context = nullptr;
	}
	if (nullptr != m_socket)
	{
		zmq_close(m_socket);
		m_socket = nullptr;
	}
}

bool ZmqCommon::Front()
{
	int nRet = 0;
	m_context = zmq_init(1);    //指定zmq 处理I/0事件的thread pool 为1
	if (nullptr == m_context)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		return false;
	}
	m_socket = zmq_socket(m_context, ZMQ_REQ);
	if (nullptr == m_socket)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		zmq_term(m_context);
		return false;
	}

	nRet = zmq_setsockopt(m_socket, ZMQ_SNDTIMEO, &m_uintTimeout, sizeof(m_uintTimeout));
	if (nRet < 0)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		zmq_close(m_socket);
		zmq_term(m_context);
		return false;
	}

	return true;
}

bool ZmqCommon::Send(std::shared_ptr<char> pBuffer, int nLen)
{
	//组建发送包
	int nRet = 0;
	zmq_msg_t sendMsg;
	zmq_msg_init_size(&sendMsg, nLen + 4);

	memcpy((char*)zmq_msg_data(&sendMsg) + 4, pBuffer.get(), nLen);

	//连接到服务器
	Sleep(500);
	zmq_disconnect(m_socket, m_strConnect.c_str());
	nRet = zmq_connect(m_socket, m_strConnect.c_str());
	if (nRet < 0)
	{
		return false;
	}

	//发送数据
	Sleep(500);
	nRet = zmq_msg_send(&sendMsg, m_socket, 0);
	if (nRet < 0)
	{
		return false;
	}
	zmq_msg_close(&sendMsg);

	return true;
}

bool ZmqCommon::Recv(std::shared_ptr<char> pBuffer, int &nLen, int nBufferSize)
{
	int nRet = 0;
	zmq_msg_t recvMsg;
	zmq_msg_init(&recvMsg);
	nRet = zmq_msg_recv( &recvMsg, m_socket, 0);
	if (nRet < 0)
	{
		int nError = zmq_errno();
		printf("%s", zmq_strerror(nError));
		nLen = 0;
		return false;
	}
	nLen = zmq_msg_size(&recvMsg);

	if (nLen > nBufferSize)
	{
		memcpy(pBuffer.get(), zmq_msg_data(&recvMsg), nBufferSize);
	}
	else
	{
		memcpy(pBuffer.get(), zmq_msg_data(&recvMsg), nLen);
	}
	return true;
}

bool ZmqCommon::Back()
{
	if (nullptr != m_context)
	{
		zmq_term(m_context);
		m_context = nullptr;
	}
	if (nullptr != m_socket)
	{
		zmq_close(m_socket);
		m_socket = nullptr;
	}

	return true;
}