#include "stdafx.h"
#include "ServiceManager.h"
#include <google\protobuf\descriptor.h>

ServiceManager::ServiceManager()
{
}


ServiceManager::~ServiceManager()
{
}

bool ServiceManager::RegisterRpcService(google::protobuf::Service *rpc_service)
{
	const google::protobuf::ServiceDescriptor *serviceDescriptor = rpc_service->GetDescriptor();
	std::string strServiceName = serviceDescriptor->name();
	auto iter = m_mapService.find(strServiceName);
	if (iter != m_mapService.end())
	{
		return  false;
	}
	m_mapService[strServiceName] = rpc_service;
	return true;
}

google::protobuf::Service *ServiceManager::GetService(std::string service)
{
	auto iter = m_mapService.find(service);
	if (iter == m_mapService.end())
	{
		return  nullptr;
	}
	google::protobuf::Service *objService = m_mapService[service];

	return objService;
}

google::protobuf::MethodDescriptor* ServiceManager::GetMethod(std::string service, std::string method)
{
	google::protobuf::Service *serviceData = m_mapService[service];
	if (nullptr == serviceData)
	{
		return nullptr;
	}

	const google::protobuf::ServiceDescriptor *serviceDescriptor = serviceData->GetDescriptor();

	for (int i = 0; i < serviceDescriptor->method_count(); i++)
	{
		google::protobuf::MethodDescriptor *methodDescriptor = (google::protobuf::MethodDescriptor *)serviceDescriptor->method(i);
		if (methodDescriptor->name() == method)
		{
			return methodDescriptor;
		}
	}
	return nullptr;
}
