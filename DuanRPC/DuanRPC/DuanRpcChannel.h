#pragma once

#include <google\protobuf\service.h>

#include "BaseClientCommon.h"

class CDuanRpcChannel : public google::protobuf::RpcChannel
{
public:
	CDuanRpcChannel(std::shared_ptr<BaseClientCommon> Common);
	~CDuanRpcChannel();
	virtual void CallMethod(const google::protobuf::MethodDescriptor* method,
		google::protobuf::RpcController* controller,
		const google::protobuf::Message* request,
		google::protobuf::Message* response,
		google::protobuf::Closure* done);

	static uint64_t GetIdentify();

private:
	std::shared_ptr<BaseClientCommon> m_baseCommon;
};


