#pragma once
#include "RemoteCall.pb.h"
#include "DuanRpcChannel.h"
#include "DuanRpcController.h"

class CallHelloClient
{
public:
	CallHelloClient(std::shared_ptr<CDuanRpcChannel> channel) {
		stub_ = std::make_shared<Duan::CallHello_RPC_Stub>(channel.get());
	}
	~CallHelloClient() {};

	void SayHellp(google::protobuf::string strHello)
	{
		Duan::CallHello_Request request;
		Duan::CallHello_Response response;
		DuanRpcController controller;

		request.set_request(strHello);

		stub_->SayHello(&controller, &request, &response, nullptr);

		if (controller.Failed())
		{
			return;
		}
		std::string str = response.respone();
		printf("%s", str.c_str());

		return;
	}

private:
	std::shared_ptr<Duan::CallHello_RPC_Stub> stub_;
};

