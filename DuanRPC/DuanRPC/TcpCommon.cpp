#include "stdafx.h"
#include "TcpCommon.h"


TcpCommon::TcpCommon(std::string strConnect, int nPort)
{
	m_strConnect = strConnect;
	m_uintTimeout = 10 * 1000;
	m_uintPort = nPort;
}


TcpCommon::~TcpCommon()
{
}


bool TcpCommon::Front()
{
	m_nTcp.InitSocket(m_uintTimeout);
	return true;
}

bool TcpCommon::Send(std::shared_ptr<char> pBuffer, int nLen)
{
	if (m_nTcp.ConnectServer((char *)m_strConnect.c_str(), m_uintPort) < 0)
	{
		return false;
	}

	char bufLen[16];
	sprintf_s(bufLen, "%04d", nLen);
	
	if (m_nTcp.Send(bufLen, 4) <= 0)
	{
		return false;
	}

	if (m_nTcp.Send(pBuffer.get(), nLen) <= 0)
	{
		return false;
	}
	return true;
}

bool TcpCommon::Recv(std::shared_ptr<char> pBuffer, int &nLen, int nBufferSize)
{
	char buf[8] = { 0 };
	if (nLen = m_nTcp.Recv(buf, 4) <= 0)
	{
		return false;
	}

	int nDataLen = atoi(buf);
	if (nDataLen > nBufferSize)
	{
		return false;
	}

	if ((nLen = m_nTcp.Recv(pBuffer.get(), nDataLen)) <= 0)
	{
		return false;
	}
	return true;
}

bool TcpCommon::Back()
{
	return true;
}


