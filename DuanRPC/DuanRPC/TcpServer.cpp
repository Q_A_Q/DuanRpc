#include "stdafx.h"
#include "TcpServer.h"
#include "RemoteCall.pb.h"
#include "DuanRpcController.h"

TcpServer::TcpServer(int nPort, std::shared_ptr<ServiceManager> serviceManager)
{
	m_serviceManager = serviceManager;
	m_nPort = nPort;

}


TcpServer::~TcpServer()
{
}
bool TcpServer::Front()
{
	m_nTcp.CreateServer(m_nPort, false, NULL);
	return true;
}

bool TcpServer::Back()
{
	return true;
}

void TcpServer::ThreadWork(SOCKET socket)
{
	CTcp client;
	client.SetSocket(socket);

	char buf[8] = { 0 };
	if (client.Recv(buf, 4) < 0)
	{
		return;
	}

	int nLen = atoi(buf);

	char *bufRecvData = new char[nLen];
	if (client.Recv(bufRecvData, nLen) < 0)
	{
		std::shared_ptr<char> pBufRecvData(bufRecvData);
		return;
	}
	std::shared_ptr<char> pBufRecvData(bufRecvData);

	Duan::DuanRpcMessage messageRequest;
	DuanRpcController controller;
	Duan::DuanRpcMessage messageResponse;
	bool bRet = false;

	messageRequest.ParseFromArray(bufRecvData, nLen);
	std::string strService = messageRequest.service();
	std::string strMethod = messageRequest.method();

	//查找对应的接口类型
	google::protobuf::Service *service = m_serviceManager->GetService(strService);
	google::protobuf::MethodDescriptor* method = m_serviceManager->GetMethod(strService, strMethod);
	google::protobuf::Message *request = service->GetRequestPrototype(method).New();
	google::protobuf::Message *response = service->GetResponsePrototype(method).New();

	if (nullptr == service)
	{
		messageResponse.set_code(Duan::DuanRpcErrorCode::NO_SERVICE);
	}
	else if (nullptr == method)
	{
		messageResponse.set_code(Duan::DuanRpcErrorCode::NO_METHOD);
	}
	else
	{
		//开始调用
		bRet = request->ParseFromArray(messageRequest.request().c_str(), messageRequest.request().size());
		if (false == bRet)
		{
			messageResponse.set_code(Duan::DuanRpcErrorCode::INVALID_REQUEST);
		}
		service->CallMethod(method, &controller, request, response, nullptr);
	}


	//组返回报文
	messageResponse.set_type(::Duan::DuanRpcMessageType::RESPONSE);
	messageResponse.set_code(Duan::DuanRpcErrorCode::_NO_ERROR);
	messageResponse.set_identify(messageRequest.identify());

	//序列化Response
	int nByteSize = response->ByteSize();
	char *pBuffer = new char[nByteSize];
	response->SerializeToArray(pBuffer, nByteSize);
	messageResponse.set_response(pBuffer, nByteSize);
	std::shared_ptr<char> pResposneBuf(pBuffer);


	//将整个返回报文序列化
	nByteSize = messageResponse.ByteSize();
	char *pBufSend = new char[nByteSize];
	messageResponse.SerializeToArray(pBufSend, nByteSize);
	std::shared_ptr<char> pBufSendPtr(pBufSend);
	char chBufSendResponseLen[8] = { 0 };
	sprintf(chBufSendResponseLen, "%04d", nByteSize);

	//发送返回报文
	if (client.Send(chBufSendResponseLen, 4) < 0)
	{
		goto EXIT;
	}
	if (client.Send(pBufSend, nByteSize) < 0)
	{
		goto EXIT;
	}
	

	//清空参数列表
EXIT:

	request->Clear();
	response->Clear();
}

bool TcpServer::Listen()
{
	while (1) {
	 SOCKET socket = m_nTcp.AcceptConnect();
	 m_threadGroup.create_thread(boost::bind(&TcpServer::ThreadWork, this, socket));	 
	}
	
	return true;
}

