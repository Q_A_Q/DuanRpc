#pragma once
#define RECV_MSG_MAX_BUFFER 1024 * 1024 * 16
class BaseServerCommon
{
public:
	BaseServerCommon();
	~BaseServerCommon();

	//使用之前初始化操作
	virtual bool Front() = 0;

	//发送数据
	virtual bool Listen() = 0;

	//使用之后的操作
	virtual bool Back() = 0;

};

