#pragma once
#include "BaseServerCommon.h"
#include "ServiceManager.h"
#include <string>
class ZmqServer : public BaseServerCommon
{
public:
	ZmqServer(std::string strListen, std::shared_ptr<ServiceManager>);
	~ZmqServer();

	//使用之前初始化操作
	bool Front();

	//发送数据
	bool Listen();

	//使用之后的操作
	bool Back();

private:
	std::string m_strLieten;
	void *m_context;
	void *m_listenSocket;
	int m_uintTimeout;
	std::shared_ptr<ServiceManager> m_serviceManager;
};

