// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: RemoteCall.proto

#ifndef PROTOBUF_INCLUDED_RemoteCall_2eproto
#define PROTOBUF_INCLUDED_RemoteCall_2eproto

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3006000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3006000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/service.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#define PROTOBUF_INTERNAL_EXPORT_protobuf_RemoteCall_2eproto 

namespace protobuf_RemoteCall_2eproto {
// Internal implementation detail -- do not use these members.
struct TableStruct {
  static const ::google::protobuf::internal::ParseTableField entries[];
  static const ::google::protobuf::internal::AuxillaryParseTableField aux[];
  static const ::google::protobuf::internal::ParseTable schema[3];
  static const ::google::protobuf::internal::FieldMetadata field_metadata[];
  static const ::google::protobuf::internal::SerializationTable serialization_table[];
  static const ::google::protobuf::uint32 offsets[];
};
void AddDescriptors();
}  // namespace protobuf_RemoteCall_2eproto
namespace Duan {
class CallHello_Request;
class CallHello_RequestDefaultTypeInternal;
extern CallHello_RequestDefaultTypeInternal _CallHello_Request_default_instance_;
class CallHello_Response;
class CallHello_ResponseDefaultTypeInternal;
extern CallHello_ResponseDefaultTypeInternal _CallHello_Response_default_instance_;
class DuanRpcMessage;
class DuanRpcMessageDefaultTypeInternal;
extern DuanRpcMessageDefaultTypeInternal _DuanRpcMessage_default_instance_;
}  // namespace Duan
namespace google {
namespace protobuf {
template<> ::Duan::CallHello_Request* Arena::CreateMaybeMessage<::Duan::CallHello_Request>(Arena*);
template<> ::Duan::CallHello_Response* Arena::CreateMaybeMessage<::Duan::CallHello_Response>(Arena*);
template<> ::Duan::DuanRpcMessage* Arena::CreateMaybeMessage<::Duan::DuanRpcMessage>(Arena*);
}  // namespace protobuf
}  // namespace google
namespace Duan {

enum DuanRpcMessageType {
  UNKNOWN = 0,
  REQUEST = 1,
  RESPONSE = 2,
  DuanRpcMessageType_INT_MIN_SENTINEL_DO_NOT_USE_ = ::google::protobuf::kint32min,
  DuanRpcMessageType_INT_MAX_SENTINEL_DO_NOT_USE_ = ::google::protobuf::kint32max
};
bool DuanRpcMessageType_IsValid(int value);
const DuanRpcMessageType DuanRpcMessageType_MIN = UNKNOWN;
const DuanRpcMessageType DuanRpcMessageType_MAX = RESPONSE;
const int DuanRpcMessageType_ARRAYSIZE = DuanRpcMessageType_MAX + 1;

const ::google::protobuf::EnumDescriptor* DuanRpcMessageType_descriptor();
inline const ::std::string& DuanRpcMessageType_Name(DuanRpcMessageType value) {
  return ::google::protobuf::internal::NameOfEnum(
    DuanRpcMessageType_descriptor(), value);
}
inline bool DuanRpcMessageType_Parse(
    const ::std::string& name, DuanRpcMessageType* value) {
  return ::google::protobuf::internal::ParseNamedEnum<DuanRpcMessageType>(
    DuanRpcMessageType_descriptor(), name, value);
}
enum DuanRpcErrorCode {
  _NO_ERROR = 0,
  WRONG_PROTO = 1,
  NO_SERVICE = 2,
  NO_METHOD = 3,
  INVALID_REQUEST = 4,
  INVALID_RESPONSE = 5,
  TIMEOUT = 6,
  DuanRpcErrorCode_INT_MIN_SENTINEL_DO_NOT_USE_ = ::google::protobuf::kint32min,
  DuanRpcErrorCode_INT_MAX_SENTINEL_DO_NOT_USE_ = ::google::protobuf::kint32max
};
bool DuanRpcErrorCode_IsValid(int value);
const DuanRpcErrorCode DuanRpcErrorCode_MIN = _NO_ERROR;
const DuanRpcErrorCode DuanRpcErrorCode_MAX = TIMEOUT;
const int DuanRpcErrorCode_ARRAYSIZE = DuanRpcErrorCode_MAX + 1;

const ::google::protobuf::EnumDescriptor* DuanRpcErrorCode_descriptor();
inline const ::std::string& DuanRpcErrorCode_Name(DuanRpcErrorCode value) {
  return ::google::protobuf::internal::NameOfEnum(
    DuanRpcErrorCode_descriptor(), value);
}
inline bool DuanRpcErrorCode_Parse(
    const ::std::string& name, DuanRpcErrorCode* value) {
  return ::google::protobuf::internal::ParseNamedEnum<DuanRpcErrorCode>(
    DuanRpcErrorCode_descriptor(), name, value);
}
// ===================================================================

class DuanRpcMessage : public ::google::protobuf::Message /* @@protoc_insertion_point(class_definition:Duan.DuanRpcMessage) */ {
 public:
  DuanRpcMessage();
  virtual ~DuanRpcMessage();

  DuanRpcMessage(const DuanRpcMessage& from);

  inline DuanRpcMessage& operator=(const DuanRpcMessage& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  DuanRpcMessage(DuanRpcMessage&& from) noexcept
    : DuanRpcMessage() {
    *this = ::std::move(from);
  }

  inline DuanRpcMessage& operator=(DuanRpcMessage&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  static const ::google::protobuf::Descriptor* descriptor();
  static const DuanRpcMessage& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const DuanRpcMessage* internal_default_instance() {
    return reinterpret_cast<const DuanRpcMessage*>(
               &_DuanRpcMessage_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  void Swap(DuanRpcMessage* other);
  friend void swap(DuanRpcMessage& a, DuanRpcMessage& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline DuanRpcMessage* New() const final {
    return CreateMaybeMessage<DuanRpcMessage>(NULL);
  }

  DuanRpcMessage* New(::google::protobuf::Arena* arena) const final {
    return CreateMaybeMessage<DuanRpcMessage>(arena);
  }
  void CopyFrom(const ::google::protobuf::Message& from) final;
  void MergeFrom(const ::google::protobuf::Message& from) final;
  void CopyFrom(const DuanRpcMessage& from);
  void MergeFrom(const DuanRpcMessage& from);
  void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) final;
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const final;
  ::google::protobuf::uint8* InternalSerializeWithCachedSizesToArray(
      bool deterministic, ::google::protobuf::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(DuanRpcMessage* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return NULL;
  }
  inline void* MaybeArenaPtr() const {
    return NULL;
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // string service = 2;
  void clear_service();
  static const int kServiceFieldNumber = 2;
  const ::std::string& service() const;
  void set_service(const ::std::string& value);
  #if LANG_CXX11
  void set_service(::std::string&& value);
  #endif
  void set_service(const char* value);
  void set_service(const char* value, size_t size);
  ::std::string* mutable_service();
  ::std::string* release_service();
  void set_allocated_service(::std::string* service);

  // string method = 3;
  void clear_method();
  static const int kMethodFieldNumber = 3;
  const ::std::string& method() const;
  void set_method(const ::std::string& value);
  #if LANG_CXX11
  void set_method(::std::string&& value);
  #endif
  void set_method(const char* value);
  void set_method(const char* value, size_t size);
  ::std::string* mutable_method();
  ::std::string* release_method();
  void set_allocated_method(::std::string* method);

  // bytes request = 4;
  void clear_request();
  static const int kRequestFieldNumber = 4;
  const ::std::string& request() const;
  void set_request(const ::std::string& value);
  #if LANG_CXX11
  void set_request(::std::string&& value);
  #endif
  void set_request(const char* value);
  void set_request(const void* value, size_t size);
  ::std::string* mutable_request();
  ::std::string* release_request();
  void set_allocated_request(::std::string* request);

  // bytes response = 5;
  void clear_response();
  static const int kResponseFieldNumber = 5;
  const ::std::string& response() const;
  void set_response(const ::std::string& value);
  #if LANG_CXX11
  void set_response(::std::string&& value);
  #endif
  void set_response(const char* value);
  void set_response(const void* value, size_t size);
  ::std::string* mutable_response();
  ::std::string* release_response();
  void set_allocated_response(::std::string* response);

  // .Duan.DuanRpcMessageType type = 1;
  void clear_type();
  static const int kTypeFieldNumber = 1;
  ::Duan::DuanRpcMessageType type() const;
  void set_type(::Duan::DuanRpcMessageType value);

  // .Duan.DuanRpcErrorCode code = 6;
  void clear_code();
  static const int kCodeFieldNumber = 6;
  ::Duan::DuanRpcErrorCode code() const;
  void set_code(::Duan::DuanRpcErrorCode value);

  // uint64 identify = 7;
  void clear_identify();
  static const int kIdentifyFieldNumber = 7;
  ::google::protobuf::uint64 identify() const;
  void set_identify(::google::protobuf::uint64 value);

  // @@protoc_insertion_point(class_scope:Duan.DuanRpcMessage)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::internal::ArenaStringPtr service_;
  ::google::protobuf::internal::ArenaStringPtr method_;
  ::google::protobuf::internal::ArenaStringPtr request_;
  ::google::protobuf::internal::ArenaStringPtr response_;
  int type_;
  int code_;
  ::google::protobuf::uint64 identify_;
  mutable ::google::protobuf::internal::CachedSize _cached_size_;
  friend struct ::protobuf_RemoteCall_2eproto::TableStruct;
};
// -------------------------------------------------------------------

class CallHello_Request : public ::google::protobuf::Message /* @@protoc_insertion_point(class_definition:Duan.CallHello_Request) */ {
 public:
  CallHello_Request();
  virtual ~CallHello_Request();

  CallHello_Request(const CallHello_Request& from);

  inline CallHello_Request& operator=(const CallHello_Request& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  CallHello_Request(CallHello_Request&& from) noexcept
    : CallHello_Request() {
    *this = ::std::move(from);
  }

  inline CallHello_Request& operator=(CallHello_Request&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  static const ::google::protobuf::Descriptor* descriptor();
  static const CallHello_Request& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const CallHello_Request* internal_default_instance() {
    return reinterpret_cast<const CallHello_Request*>(
               &_CallHello_Request_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    1;

  void Swap(CallHello_Request* other);
  friend void swap(CallHello_Request& a, CallHello_Request& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline CallHello_Request* New() const final {
    return CreateMaybeMessage<CallHello_Request>(NULL);
  }

  CallHello_Request* New(::google::protobuf::Arena* arena) const final {
    return CreateMaybeMessage<CallHello_Request>(arena);
  }
  void CopyFrom(const ::google::protobuf::Message& from) final;
  void MergeFrom(const ::google::protobuf::Message& from) final;
  void CopyFrom(const CallHello_Request& from);
  void MergeFrom(const CallHello_Request& from);
  void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) final;
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const final;
  ::google::protobuf::uint8* InternalSerializeWithCachedSizesToArray(
      bool deterministic, ::google::protobuf::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(CallHello_Request* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return NULL;
  }
  inline void* MaybeArenaPtr() const {
    return NULL;
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // string request = 1;
  void clear_request();
  static const int kRequestFieldNumber = 1;
  const ::std::string& request() const;
  void set_request(const ::std::string& value);
  #if LANG_CXX11
  void set_request(::std::string&& value);
  #endif
  void set_request(const char* value);
  void set_request(const char* value, size_t size);
  ::std::string* mutable_request();
  ::std::string* release_request();
  void set_allocated_request(::std::string* request);

  // @@protoc_insertion_point(class_scope:Duan.CallHello_Request)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::internal::ArenaStringPtr request_;
  mutable ::google::protobuf::internal::CachedSize _cached_size_;
  friend struct ::protobuf_RemoteCall_2eproto::TableStruct;
};
// -------------------------------------------------------------------

class CallHello_Response : public ::google::protobuf::Message /* @@protoc_insertion_point(class_definition:Duan.CallHello_Response) */ {
 public:
  CallHello_Response();
  virtual ~CallHello_Response();

  CallHello_Response(const CallHello_Response& from);

  inline CallHello_Response& operator=(const CallHello_Response& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  CallHello_Response(CallHello_Response&& from) noexcept
    : CallHello_Response() {
    *this = ::std::move(from);
  }

  inline CallHello_Response& operator=(CallHello_Response&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  static const ::google::protobuf::Descriptor* descriptor();
  static const CallHello_Response& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const CallHello_Response* internal_default_instance() {
    return reinterpret_cast<const CallHello_Response*>(
               &_CallHello_Response_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    2;

  void Swap(CallHello_Response* other);
  friend void swap(CallHello_Response& a, CallHello_Response& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline CallHello_Response* New() const final {
    return CreateMaybeMessage<CallHello_Response>(NULL);
  }

  CallHello_Response* New(::google::protobuf::Arena* arena) const final {
    return CreateMaybeMessage<CallHello_Response>(arena);
  }
  void CopyFrom(const ::google::protobuf::Message& from) final;
  void MergeFrom(const ::google::protobuf::Message& from) final;
  void CopyFrom(const CallHello_Response& from);
  void MergeFrom(const CallHello_Response& from);
  void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) final;
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const final;
  ::google::protobuf::uint8* InternalSerializeWithCachedSizesToArray(
      bool deterministic, ::google::protobuf::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(CallHello_Response* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return NULL;
  }
  inline void* MaybeArenaPtr() const {
    return NULL;
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // string respone = 1;
  void clear_respone();
  static const int kResponeFieldNumber = 1;
  const ::std::string& respone() const;
  void set_respone(const ::std::string& value);
  #if LANG_CXX11
  void set_respone(::std::string&& value);
  #endif
  void set_respone(const char* value);
  void set_respone(const char* value, size_t size);
  ::std::string* mutable_respone();
  ::std::string* release_respone();
  void set_allocated_respone(::std::string* respone);

  // @@protoc_insertion_point(class_scope:Duan.CallHello_Response)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::internal::ArenaStringPtr respone_;
  mutable ::google::protobuf::internal::CachedSize _cached_size_;
  friend struct ::protobuf_RemoteCall_2eproto::TableStruct;
};
// ===================================================================

class CallHello_RPC_Stub;

class CallHello_RPC : public ::google::protobuf::Service {
 protected:
  // This class should be treated as an abstract interface.
  inline CallHello_RPC() {};
 public:
  virtual ~CallHello_RPC();

  typedef CallHello_RPC_Stub Stub;

  static const ::google::protobuf::ServiceDescriptor* descriptor();

  virtual void SayHello(::google::protobuf::RpcController* controller,
                       const ::Duan::CallHello_Request* request,
                       ::Duan::CallHello_Response* response,
                       ::google::protobuf::Closure* done);
  virtual void GetServerTime(::google::protobuf::RpcController* controller,
                       const ::Duan::CallHello_Request* request,
                       ::Duan::CallHello_Response* response,
                       ::google::protobuf::Closure* done);

  // implements Service ----------------------------------------------

  const ::google::protobuf::ServiceDescriptor* GetDescriptor();
  void CallMethod(const ::google::protobuf::MethodDescriptor* method,
                  ::google::protobuf::RpcController* controller,
                  const ::google::protobuf::Message* request,
                  ::google::protobuf::Message* response,
                  ::google::protobuf::Closure* done);
  const ::google::protobuf::Message& GetRequestPrototype(
    const ::google::protobuf::MethodDescriptor* method) const;
  const ::google::protobuf::Message& GetResponsePrototype(
    const ::google::protobuf::MethodDescriptor* method) const;

 private:
  GOOGLE_DISALLOW_EVIL_CONSTRUCTORS(CallHello_RPC);
};

class CallHello_RPC_Stub : public CallHello_RPC {
 public:
  CallHello_RPC_Stub(::google::protobuf::RpcChannel* channel);
  CallHello_RPC_Stub(::google::protobuf::RpcChannel* channel,
                   ::google::protobuf::Service::ChannelOwnership ownership);
  ~CallHello_RPC_Stub();

  inline ::google::protobuf::RpcChannel* channel() { return channel_; }

  // implements CallHello_RPC ------------------------------------------

  void SayHello(::google::protobuf::RpcController* controller,
                       const ::Duan::CallHello_Request* request,
                       ::Duan::CallHello_Response* response,
                       ::google::protobuf::Closure* done);
  void GetServerTime(::google::protobuf::RpcController* controller,
                       const ::Duan::CallHello_Request* request,
                       ::Duan::CallHello_Response* response,
                       ::google::protobuf::Closure* done);
 private:
  ::google::protobuf::RpcChannel* channel_;
  bool owns_channel_;
  GOOGLE_DISALLOW_EVIL_CONSTRUCTORS(CallHello_RPC_Stub);
};


// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// DuanRpcMessage

// .Duan.DuanRpcMessageType type = 1;
inline void DuanRpcMessage::clear_type() {
  type_ = 0;
}
inline ::Duan::DuanRpcMessageType DuanRpcMessage::type() const {
  // @@protoc_insertion_point(field_get:Duan.DuanRpcMessage.type)
  return static_cast< ::Duan::DuanRpcMessageType >(type_);
}
inline void DuanRpcMessage::set_type(::Duan::DuanRpcMessageType value) {
  
  type_ = value;
  // @@protoc_insertion_point(field_set:Duan.DuanRpcMessage.type)
}

// string service = 2;
inline void DuanRpcMessage::clear_service() {
  service_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& DuanRpcMessage::service() const {
  // @@protoc_insertion_point(field_get:Duan.DuanRpcMessage.service)
  return service_.GetNoArena();
}
inline void DuanRpcMessage::set_service(const ::std::string& value) {
  
  service_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:Duan.DuanRpcMessage.service)
}
#if LANG_CXX11
inline void DuanRpcMessage::set_service(::std::string&& value) {
  
  service_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:Duan.DuanRpcMessage.service)
}
#endif
inline void DuanRpcMessage::set_service(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  service_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:Duan.DuanRpcMessage.service)
}
inline void DuanRpcMessage::set_service(const char* value, size_t size) {
  
  service_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:Duan.DuanRpcMessage.service)
}
inline ::std::string* DuanRpcMessage::mutable_service() {
  
  // @@protoc_insertion_point(field_mutable:Duan.DuanRpcMessage.service)
  return service_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* DuanRpcMessage::release_service() {
  // @@protoc_insertion_point(field_release:Duan.DuanRpcMessage.service)
  
  return service_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void DuanRpcMessage::set_allocated_service(::std::string* service) {
  if (service != NULL) {
    
  } else {
    
  }
  service_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), service);
  // @@protoc_insertion_point(field_set_allocated:Duan.DuanRpcMessage.service)
}

// string method = 3;
inline void DuanRpcMessage::clear_method() {
  method_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& DuanRpcMessage::method() const {
  // @@protoc_insertion_point(field_get:Duan.DuanRpcMessage.method)
  return method_.GetNoArena();
}
inline void DuanRpcMessage::set_method(const ::std::string& value) {
  
  method_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:Duan.DuanRpcMessage.method)
}
#if LANG_CXX11
inline void DuanRpcMessage::set_method(::std::string&& value) {
  
  method_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:Duan.DuanRpcMessage.method)
}
#endif
inline void DuanRpcMessage::set_method(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  method_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:Duan.DuanRpcMessage.method)
}
inline void DuanRpcMessage::set_method(const char* value, size_t size) {
  
  method_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:Duan.DuanRpcMessage.method)
}
inline ::std::string* DuanRpcMessage::mutable_method() {
  
  // @@protoc_insertion_point(field_mutable:Duan.DuanRpcMessage.method)
  return method_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* DuanRpcMessage::release_method() {
  // @@protoc_insertion_point(field_release:Duan.DuanRpcMessage.method)
  
  return method_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void DuanRpcMessage::set_allocated_method(::std::string* method) {
  if (method != NULL) {
    
  } else {
    
  }
  method_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), method);
  // @@protoc_insertion_point(field_set_allocated:Duan.DuanRpcMessage.method)
}

// bytes request = 4;
inline void DuanRpcMessage::clear_request() {
  request_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& DuanRpcMessage::request() const {
  // @@protoc_insertion_point(field_get:Duan.DuanRpcMessage.request)
  return request_.GetNoArena();
}
inline void DuanRpcMessage::set_request(const ::std::string& value) {
  
  request_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:Duan.DuanRpcMessage.request)
}
#if LANG_CXX11
inline void DuanRpcMessage::set_request(::std::string&& value) {
  
  request_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:Duan.DuanRpcMessage.request)
}
#endif
inline void DuanRpcMessage::set_request(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  request_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:Duan.DuanRpcMessage.request)
}
inline void DuanRpcMessage::set_request(const void* value, size_t size) {
  
  request_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:Duan.DuanRpcMessage.request)
}
inline ::std::string* DuanRpcMessage::mutable_request() {
  
  // @@protoc_insertion_point(field_mutable:Duan.DuanRpcMessage.request)
  return request_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* DuanRpcMessage::release_request() {
  // @@protoc_insertion_point(field_release:Duan.DuanRpcMessage.request)
  
  return request_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void DuanRpcMessage::set_allocated_request(::std::string* request) {
  if (request != NULL) {
    
  } else {
    
  }
  request_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), request);
  // @@protoc_insertion_point(field_set_allocated:Duan.DuanRpcMessage.request)
}

// bytes response = 5;
inline void DuanRpcMessage::clear_response() {
  response_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& DuanRpcMessage::response() const {
  // @@protoc_insertion_point(field_get:Duan.DuanRpcMessage.response)
  return response_.GetNoArena();
}
inline void DuanRpcMessage::set_response(const ::std::string& value) {
  
  response_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:Duan.DuanRpcMessage.response)
}
#if LANG_CXX11
inline void DuanRpcMessage::set_response(::std::string&& value) {
  
  response_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:Duan.DuanRpcMessage.response)
}
#endif
inline void DuanRpcMessage::set_response(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  response_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:Duan.DuanRpcMessage.response)
}
inline void DuanRpcMessage::set_response(const void* value, size_t size) {
  
  response_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:Duan.DuanRpcMessage.response)
}
inline ::std::string* DuanRpcMessage::mutable_response() {
  
  // @@protoc_insertion_point(field_mutable:Duan.DuanRpcMessage.response)
  return response_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* DuanRpcMessage::release_response() {
  // @@protoc_insertion_point(field_release:Duan.DuanRpcMessage.response)
  
  return response_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void DuanRpcMessage::set_allocated_response(::std::string* response) {
  if (response != NULL) {
    
  } else {
    
  }
  response_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), response);
  // @@protoc_insertion_point(field_set_allocated:Duan.DuanRpcMessage.response)
}

// .Duan.DuanRpcErrorCode code = 6;
inline void DuanRpcMessage::clear_code() {
  code_ = 0;
}
inline ::Duan::DuanRpcErrorCode DuanRpcMessage::code() const {
  // @@protoc_insertion_point(field_get:Duan.DuanRpcMessage.code)
  return static_cast< ::Duan::DuanRpcErrorCode >(code_);
}
inline void DuanRpcMessage::set_code(::Duan::DuanRpcErrorCode value) {
  
  code_ = value;
  // @@protoc_insertion_point(field_set:Duan.DuanRpcMessage.code)
}

// uint64 identify = 7;
inline void DuanRpcMessage::clear_identify() {
  identify_ = GOOGLE_ULONGLONG(0);
}
inline ::google::protobuf::uint64 DuanRpcMessage::identify() const {
  // @@protoc_insertion_point(field_get:Duan.DuanRpcMessage.identify)
  return identify_;
}
inline void DuanRpcMessage::set_identify(::google::protobuf::uint64 value) {
  
  identify_ = value;
  // @@protoc_insertion_point(field_set:Duan.DuanRpcMessage.identify)
}

// -------------------------------------------------------------------

// CallHello_Request

// string request = 1;
inline void CallHello_Request::clear_request() {
  request_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& CallHello_Request::request() const {
  // @@protoc_insertion_point(field_get:Duan.CallHello_Request.request)
  return request_.GetNoArena();
}
inline void CallHello_Request::set_request(const ::std::string& value) {
  
  request_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:Duan.CallHello_Request.request)
}
#if LANG_CXX11
inline void CallHello_Request::set_request(::std::string&& value) {
  
  request_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:Duan.CallHello_Request.request)
}
#endif
inline void CallHello_Request::set_request(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  request_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:Duan.CallHello_Request.request)
}
inline void CallHello_Request::set_request(const char* value, size_t size) {
  
  request_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:Duan.CallHello_Request.request)
}
inline ::std::string* CallHello_Request::mutable_request() {
  
  // @@protoc_insertion_point(field_mutable:Duan.CallHello_Request.request)
  return request_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* CallHello_Request::release_request() {
  // @@protoc_insertion_point(field_release:Duan.CallHello_Request.request)
  
  return request_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void CallHello_Request::set_allocated_request(::std::string* request) {
  if (request != NULL) {
    
  } else {
    
  }
  request_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), request);
  // @@protoc_insertion_point(field_set_allocated:Duan.CallHello_Request.request)
}

// -------------------------------------------------------------------

// CallHello_Response

// string respone = 1;
inline void CallHello_Response::clear_respone() {
  respone_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& CallHello_Response::respone() const {
  // @@protoc_insertion_point(field_get:Duan.CallHello_Response.respone)
  return respone_.GetNoArena();
}
inline void CallHello_Response::set_respone(const ::std::string& value) {
  
  respone_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:Duan.CallHello_Response.respone)
}
#if LANG_CXX11
inline void CallHello_Response::set_respone(::std::string&& value) {
  
  respone_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:Duan.CallHello_Response.respone)
}
#endif
inline void CallHello_Response::set_respone(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  respone_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:Duan.CallHello_Response.respone)
}
inline void CallHello_Response::set_respone(const char* value, size_t size) {
  
  respone_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:Duan.CallHello_Response.respone)
}
inline ::std::string* CallHello_Response::mutable_respone() {
  
  // @@protoc_insertion_point(field_mutable:Duan.CallHello_Response.respone)
  return respone_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* CallHello_Response::release_respone() {
  // @@protoc_insertion_point(field_release:Duan.CallHello_Response.respone)
  
  return respone_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void CallHello_Response::set_allocated_respone(::std::string* respone) {
  if (respone != NULL) {
    
  } else {
    
  }
  respone_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), respone);
  // @@protoc_insertion_point(field_set_allocated:Duan.CallHello_Response.respone)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__
// -------------------------------------------------------------------

// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace Duan

namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::Duan::DuanRpcMessageType> : ::std::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::Duan::DuanRpcMessageType>() {
  return ::Duan::DuanRpcMessageType_descriptor();
}
template <> struct is_proto_enum< ::Duan::DuanRpcErrorCode> : ::std::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::Duan::DuanRpcErrorCode>() {
  return ::Duan::DuanRpcErrorCode_descriptor();
}

}  // namespace protobuf
}  // namespace google

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_INCLUDED_RemoteCall_2eproto
