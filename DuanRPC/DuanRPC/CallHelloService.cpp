#include "stdafx.h"
#include "CallHelloService.h"


CallHelloService::~CallHelloService()
{
}


void CallHelloService::SayHello(::google::protobuf::RpcController* controller,
	const Duan::CallHello_Request* request,
	Duan::CallHello_Response* response,
	::google::protobuf::Closure* done)
{
	std::string str = request->request();
	printf("%s", str.c_str());

	response->set_respone("Hello World Response");

	return;

}