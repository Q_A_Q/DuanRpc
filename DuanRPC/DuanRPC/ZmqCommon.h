#pragma once
#include "BaseClientCommon.h"
#include <memory>
#include <string>
#include <zmq.h>

class ZmqCommon :
	public BaseClientCommon
{
public:
	ZmqCommon(std::string strConnect);
	~ZmqCommon();

	bool Front();

	bool Send(std::shared_ptr<char> pBuffer, int nLen);

	bool Recv(std::shared_ptr<char> pBuffer, int &nLen, int nBufferSize);
	
	bool Back();

private:
	std::string m_strConnect;
	void *m_context;
	void *m_socket;
	UINT m_uintTimeout;
};

