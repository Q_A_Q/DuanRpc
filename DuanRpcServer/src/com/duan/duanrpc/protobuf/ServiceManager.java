package com.duan.duanrpc.protobuf;

import com.duan.duanrpc.RpcServerService.RegisterService;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Method;
import com.google.protobuf.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceManager {
    private static final Logger logger = LoggerFactory.getLogger(ServiceManager.class);
    public static Map<String, com.google.protobuf.Service> mapRegisterService;
    static {
        mapRegisterService = new HashMap<String, Service>();
    }

    public static boolean RegisterRpcService(com.google.protobuf.Service service) {
        Descriptors.ServiceDescriptor serviceDescriptor = service.getDescriptorForType();
        String stringServiceName = serviceDescriptor.getName();

        com.google.protobuf.Service objService = mapRegisterService.get(stringServiceName);
        if (null != objService) {
            logger.error("服务已经户注册: " + stringServiceName);
            return  false;
        }
        mapRegisterService.put(stringServiceName, service);
        if (logger.isDebugEnabled()) {
            logger.debug("服务注册成功: " + stringServiceName);
        }
        return  true;
    }

    public static  com.google.protobuf.Service GetService(String serviceName) {
        return mapRegisterService.get(serviceName);
    }

    public static  Descriptors.MethodDescriptor GetMethod(String serviceName, String methodName) {
        com.google.protobuf.Service service = mapRegisterService.get(serviceName);
        if (null == service) {
            return null;
        }

        com.google.protobuf.Descriptors.ServiceDescriptor serviceDescriptor =  service.getDescriptorForType();
        List<Descriptors.MethodDescriptor > methodList = serviceDescriptor.getMethods();
        for (Descriptors.MethodDescriptor method : methodList) {
            if (method.getName().equals( methodName)) {
                return method;
            }
        }
        return null;
    }
}
