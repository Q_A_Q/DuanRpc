package com.duan.duanrpc.protobuf;

import com.google.protobuf.RpcCallback;
import com.google.protobuf.RpcController;

public class ServiceController implements RpcController {
    private boolean bIsFailed;
    private String strFailedMsg;

    @Override
    public String errorText() {
        return strFailedMsg;
    }

    @Override
    public boolean failed() {
        return bIsFailed;
    }

    @Override
    public void startCancel() {

    }

    @Override
    public void notifyOnCancel(RpcCallback<Object> rpcCallback) {

    }

    @Override
    public void setFailed(String s) {
        bIsFailed = false;
        strFailedMsg = s;
    }

    @Override
    public boolean isCanceled() {
        return false;
    }

    @Override
    public void reset() {
        bIsFailed = false;
    }
}
