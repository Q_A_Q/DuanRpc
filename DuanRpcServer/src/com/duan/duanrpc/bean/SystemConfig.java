package com.duan.duanrpc.bean;

import java.util.List;

public class SystemConfig {

    private static int maxLengthTransportMessage;

    private static  int nListenPort;

    private  static  boolean bDebug;

    private  static List<String> listDebugParam;

    private  static String strPathDebug;

    public static String getStrPathDebug() {
        return strPathDebug;
    }

    public static void setStrPathDebug(String strPathDebug) {
        SystemConfig.strPathDebug = strPathDebug;
    }

    public static List<String> getListDebugParam() {
        return listDebugParam;
    }

    public static void setListDebugParam(List<String> listDebugParam) {
        SystemConfig.listDebugParam = listDebugParam;
    }

    public static boolean isbDebug() {
        return bDebug;
    }

    public static void setbDebug(boolean bDebug) {
        SystemConfig.bDebug = bDebug;
    }

    public static int getnListenPort() {
        return nListenPort;
    }

    public static void setnListenPort(int nListenPort) {
        SystemConfig.nListenPort = nListenPort;
    }

    public static int getMaxLengthTransportMessage() {
        return maxLengthTransportMessage;
    }

    public static void setMaxLengthTransportMessage(int maxLengthTransportMessage) {
        SystemConfig.maxLengthTransportMessage = maxLengthTransportMessage;
    }
}
