package com.duan.duanrpc.transport;

import com.duan.duanrpc.bean.SystemConfig;
import com.duan.duanrpc.protobuf.RemoteCall;
import com.duan.duanrpc.protobuf.ServiceController;
import com.duan.duanrpc.protobuf.ServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.UUID;

public class DuanRpcServerHandler implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(DuanRpcServerHandler.class);
    private Socket socket;

    DuanRpcServerHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            final OutputStream outputStream = socket.getOutputStream();

            byte[] byteMessageLength = new byte[4];
            inputStream.read(byteMessageLength);
            String strByteMessageLength = new String(byteMessageLength);
            int nMessageLength =  Integer.parseInt(strByteMessageLength);
            if (nMessageLength > SystemConfig.getMaxLengthTransportMessage()) {
                logger.error("消息超过最大长度");
                return ;
            }

            //接受消息，并且将消息反序列化
            byte[] message = new byte[nMessageLength];
            inputStream.read(message);
            RemoteCall.DuanRpcMessage  duanRpcMessage = null;
            try {
                duanRpcMessage = RemoteCall.DuanRpcMessage.parseFrom(message);
            } catch (Exception e) {
                //byteBuf.resetReaderIndex(); //可能是消息没有发送完全,抛弃是比较好的做法
                logger.error("消息不能做反序列化: " + e.getStackTrace());
                return;
            }
            if (null == duanRpcMessage) {
                logger.error("消息格式不遵守protobuf");
                return ;
            }



            //判断参数
            com.duan.duanrpc.protobuf.RemoteCall.DuanRpcMessageType  type = duanRpcMessage.getType();
            if (RemoteCall.DuanRpcMessageType.REQUEST != type) {
                logger.error("报文不是一个请求报文");
                return ;
            }
            String strService = duanRpcMessage.getService();
            if (null == strService) {
                logger.error("请求报文，没有service参数");
                return ;
            }
            String strMethod = duanRpcMessage.getMethod();
            if (null == strMethod) {
                logger.error("请求报文没有method");
                return ;
            }

            //对消息进行映射
            ServiceController controller = new ServiceController();
            com.google.protobuf.Service servics = ServiceManager.GetService(strService);
            if (null == servics) {
                RpcCallFailure(outputStream, RemoteCall.DuanRpcErrorCode.NO_SERVICE);
                return;
            }

            com.google.protobuf.Descriptors.MethodDescriptor method = ServiceManager.GetMethod(strService, strMethod);
            if (null == method) {
                RpcCallFailure(outputStream, RemoteCall.DuanRpcErrorCode.NO_METHOD);
                return;
            }

            com.google.protobuf.Message request = null;
            try{
                request = servics.getRequestPrototype(method).newBuilderForType().mergeFrom(duanRpcMessage.getRequest()).build();
            } catch (Exception e) {
                logger.error("参数部分解析错误");
                RpcCallFailure(outputStream, RemoteCall.DuanRpcErrorCode.INVALID_REQUEST);
                return;
            }

            //用来调试报文
            if (SystemConfig.isbDebug())
            {
                String  strFunction = strService + "." + strMethod;

                for (String str: SystemConfig.getListDebugParam())
                {
                    if (str.equals(strFunction)) {
                        String strRequest = request.toString();

                        UUID uuid = UUID.randomUUID();
                        String strPath = SystemConfig.getStrPathDebug() + "/" + strFunction + uuid.toString();
                        File file = new File(strPath);
                        FileOutputStream out = new FileOutputStream(file);
                        out.write(strRequest.getBytes());
                    }
                }
            }

            com.google.protobuf.RpcCallback<com.google.protobuf.Message> response = new com.google.protobuf.RpcCallback<com.google.protobuf.Message>(){
                @Override
                public void run(com.google.protobuf.Message message) { //发送返回报文
                    RemoteCall.DuanRpcMessage.Builder duanRpcMessage = RemoteCall.DuanRpcMessage.newBuilder();
                    duanRpcMessage.setType(RemoteCall.DuanRpcMessageType.RESPONSE);
                    duanRpcMessage.setCode(RemoteCall.DuanRpcErrorCode._NO_ERROR);
                    byte[] bytesResponse = message.toByteArray();
                    duanRpcMessage.setResponse(com.google.protobuf.ByteString.copyFrom(bytesResponse));

                    RemoteCall.DuanRpcMessage msg = duanRpcMessage.build();
                    byte[] bytesData = msg.toByteArray();
                    String strLength = String.format("%04d", bytesData.length);
                    try{
                        outputStream.write(strLength.getBytes());
                        outputStream.write(bytesData);
                    } catch (Exception e) {
                        logger.error(e.getMessage() + e.getStackTrace());
                    }


                }
            };
            servics.callMethod(method, controller, request, response);

        } catch (Exception e) {
            logger.error(e.getMessage() + e.getStackTrace());
        }
    }

    public void RpcCallFailure(OutputStream out, RemoteCall.DuanRpcErrorCode errorCode) throws  Exception {
        RemoteCall.DuanRpcMessage.Builder duanRpcMessage = RemoteCall.DuanRpcMessage.newBuilder();
        duanRpcMessage.setType(RemoteCall.DuanRpcMessageType.RESPONSE);
        duanRpcMessage.setCode(errorCode);

        RemoteCall.DuanRpcMessage msg = duanRpcMessage.build();

        byte[] bytesData = msg.toByteArray();
        String strLength = String.format("%04d", bytesData.length);
        out.write(strLength.getBytes());
        out.write(bytesData);
        return;
    }
}
