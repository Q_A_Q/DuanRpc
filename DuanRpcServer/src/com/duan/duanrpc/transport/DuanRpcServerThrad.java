package com.duan.duanrpc.transport;

import com.duan.duanrpc.bean.SystemConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DuanRpcServerThrad implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(DuanRpcServerThrad.class);
    private static  boolean bRunning = true;
    private static  ExecutorService executorService;

    static {
        executorService = Executors.newCachedThreadPool();
    }

    @Override
    public void run() {

        try {
            ServerSocket serverSocket = new ServerSocket(SystemConfig.getnListenPort());

            while (bRunning) {
               Socket socket =  serverSocket.accept();
                executorService.execute(new DuanRpcServerHandler(socket));
            }
        } catch (Exception e) {

        }

    }
}
