package com.duan.duanrpc;

import com.duan.duanrpc.RpcServerService.RegisterService;
import com.duan.duanrpc.config.LoadDuanRpcConfig;
import com.duan.duanrpc.transport.DuanRpcServerStart;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        LoadDuanRpcConfig.LoadConfig();
        RegisterService.RegisterAllService();

        DuanRpcServerStart.StartServer();
        while(true) {
            Scanner input = new Scanner(System.in);
            char c;
            String s = input.nextLine();
            if ("q".equals(s) || "Q".equals(s)) {
                break;
            }
        }

    }
}
