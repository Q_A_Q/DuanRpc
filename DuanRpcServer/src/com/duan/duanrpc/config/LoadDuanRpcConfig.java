package com.duan.duanrpc.config;

import com.duan.duanrpc.bean.SystemConfig;

public class LoadDuanRpcConfig {

    public static void LoadConfig() {
        SystemConfig config = new SystemConfig();
        config.setMaxLengthTransportMessage(1024 * 1024 * 25);
        config.setnListenPort(8887);
    }
}
