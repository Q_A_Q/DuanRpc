package com.duan.duanrpc.RpcServerService;

import com.duan.duanrpc.protobuf.RemoteCall;
import com.google.protobuf.RpcCallback;
import com.google.protobuf.RpcController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CallHelloService extends RemoteCall.CallHello_RPC {
    private static final Logger logger = LoggerFactory.getLogger(CallHelloService.class);

    @Override
    public void sayHello(RpcController controller, RemoteCall.CallHello_Request request, RpcCallback<RemoteCall.CallHello_Response> done) {
        logger.info(request.getRequest());
        RemoteCall.CallHello_Response response = RemoteCall.CallHello_Response.newBuilder().setRespone("Hellow world response").build();
        done.run(response);
    }

    @Override
    public void getServerTime(RpcController controller, RemoteCall.CallHello_Request request, RpcCallback<RemoteCall.CallHello_Response> done) {
        logger.info("请求端时间是:" + request.getRequest());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strServerDate = df.format(new Date()).toString();
        RemoteCall.CallHello_Response response = RemoteCall.CallHello_Response.newBuilder().setRespone(strServerDate).build();
        done.run(response);
    }
}
