package com.duan.duanrpc.RpcServerService;

import com.duan.duanrpc.protobuf.ServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegisterService {
    private static final Logger logger = LoggerFactory.getLogger(RegisterService.class);
    public static void RegisterAllService() {
        ServiceManager serviceManager = new ServiceManager();

        ServiceManager.RegisterRpcService(new CallHelloService());
    }
}
