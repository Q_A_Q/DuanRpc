package test.com.duan.duanrpc.baserpc;

import com.duan.duanrpc.protobuf.RemoteCall;
import com.google.protobuf.*;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class BaseDuanRpcChannel implements RpcChannel  {
    private static final Logger logger = LoggerFactory.getLogger(BaseDuanRpcChannel.class);
    private static String ip = "127.0.0.1";
    private static  int port = 8887;

    @Override
    public void callMethod(Descriptors.MethodDescriptor methodDescriptor, RpcController rpcController, Message request, Message response, RpcCallback<Message> rpcCallback) {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Socket socket = new Socket(ip, port);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            //组建请求报文
            RemoteCall.DuanRpcMessage.Builder builder = RemoteCall.DuanRpcMessage.newBuilder();
            builder = builder.setType(RemoteCall.DuanRpcMessageType.REQUEST);
            builder = builder.setCode(RemoteCall.DuanRpcErrorCode._NO_ERROR);

            String strMethod = methodDescriptor.getName();
            String strService = methodDescriptor.getService().getName();
            builder = builder.setMethod(strMethod);
            builder = builder.setService(strService);

            byte[] bytesData = request.toByteArray();
            builder = builder.setRequest(com.google.protobuf.ByteString.copyFrom(bytesData));

            //开始发送
            byte[] dataSends = builder.build().toByteArray();
            String strLength = String.format("%04d", dataSends.length);

             outputStream.write(strLength.getBytes());
             outputStream.write(dataSends);
             outputStream.flush();


            byte [] length = new byte[4];
            while (inputStream.read(length) <=0 ) {

            }
            int nLength = Integer.parseInt(new String(length));

            byte[] byteResponse = new byte[nLength];
            inputStream.read(byteResponse);
            RemoteCall.DuanRpcMessage  duanRpcResponse = RemoteCall.DuanRpcMessage.parseFrom(byteResponse);

            if (null == duanRpcResponse) {
                logger.error("消息格式不遵守protobuf");
                return ;
            }
            com.duan.duanrpc.protobuf.RemoteCall.DuanRpcMessageType  type = duanRpcResponse.getType();
            if (RemoteCall.DuanRpcMessageType.RESPONSE != type) {
                logger.error("报文不是一个请求报文");
                return ;
            }
            com.duan.duanrpc.protobuf.RemoteCall.DuanRpcErrorCode code = duanRpcResponse.getCode();
            if (RemoteCall.DuanRpcErrorCode._NO_ERROR != code) {
                logger.error("远程调用失败 servie:" + strService + " method:" + strMethod);
                return  ;
            }

            Message.Builder reBuilder = response.toBuilder();
            reBuilder.mergeFrom(duanRpcResponse.getResponse());

            //解析response的回调函数
            rpcCallback.run(reBuilder.build());

            outputStream.close();
            inputStream.close();
            socket.close();
            //得到返回报文
            } catch (Exception e) {
            logger.info("" + e.getStackTrace());
        }

    }
}
