package test.com.duan.duanrpc.rpcclient;

import com.duan.duanrpc.protobuf.RemoteCall;
import com.duan.duanrpc.protobuf.ServiceController;
import com.google.protobuf.RpcCallback;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.com.duan.duanrpc.baserpc.BaseDuanRpcChannel;

public class CallHellowClient  {
    private static final Logger logger = LoggerFactory.getLogger(CallHellowClient.class);
    public  static RemoteCall.CallHello_RPC.Stub _stub = RemoteCall.CallHello_RPC.newStub(new BaseDuanRpcChannel());
    String strServerTime = new String();

    @Test
    public  void Test()
    {
        sayHello("Hellow World!");
        getServerTime("2018-12-13 13:18:11");
    }


    public String getServerTime(String strTime) {
      //  String strServierTime = new String();
        RemoteCall.CallHello_Request.Builder requestBuilder = RemoteCall.CallHello_Request.newBuilder();
        final RpcCallback< RemoteCall.CallHello_Response> clientCallback = new RpcCallback< RemoteCall.CallHello_Response>(){
            @Override
            public void run(RemoteCall.CallHello_Response response) {
                strServerTime = response.getRespone();
                logger.info("服务器时间是：" + response.getRespone());
            }
        };

        requestBuilder = requestBuilder.setRequest(strTime);
        _stub.getServerTime(new ServiceController(), requestBuilder.build(), clientCallback);
        return strServerTime;
    }


    public void sayHello(String strMsg) {
        RemoteCall.CallHello_Request.Builder requestBuilder = RemoteCall.CallHello_Request.newBuilder();
        final RpcCallback< RemoteCall.CallHello_Response> clientCallback = new RpcCallback< RemoteCall.CallHello_Response>(){
            @Override
            public void run(RemoteCall.CallHello_Response response) {
                logger.info("response:" + response.getRespone());
            }
        };

        requestBuilder = requestBuilder.setRequest(strMsg);
        _stub.sayHello(new ServiceController(), requestBuilder.build(), clientCallback);
    }
}
